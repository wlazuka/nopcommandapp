from selenium import webdriver
from selenium.webdriver.chrome.options import Options


class BasePage:
    options = Options()
    options.binary_location = "/usr/bin/chromium-browser"
    driver = webdriver.Chrome(options=options)

    def __init__(self, driver) -> None:
        self.driver = driver
