import datetime
import os
import pytest

from pageObjects.LoginPage import LoginPage
from utilites.readProperities import ReadConfig
from utilites.customLogger import LogGen

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


class Test_001_Login:
    baseURL = ReadConfig.getApplicationURL()
    username = ReadConfig.getUseremail()
    password = ReadConfig.getPassword()

    logger = LogGen.loggen()

    @pytest.mark.sanity
    @pytest.mark.regression
    def test_homePageTitle(self, setup):
        self.logger.info("**************** Test_001_login *****************")
        self.logger.info("************* Verifying Home Page Title *********")
        self.driver = setup
        self.driver.get(self.baseURL)
        act_title = self.driver.title
        if act_title == "Your store. Login":
            assert True
            self.logger.info("*********** Home page test is passed *********")
        else:
            self.take_screenshot(self.test_homePageTitle)
            self.logger.info("*********** Home page test is failed *********")
            assert False
        self.driver.close()

    @pytest.mark.sanity
    @pytest.mark.regression
    def test_login(self, setup):
        self.logger.info("************* Verifying Login test *********")
        self.driver = setup
        self.driver.get(self.baseURL)
        self.lp = LoginPage(self.driver)
        self.lp.setUserName(self.username)
        self.lp.setPassword(self.password)
        self.lp.clickLogin()
        act_title = self.driver.title
        if act_title == "Dashboard / nopCommerce administration":
            assert True
            self.logger.info("*********** Login test is passed *********")
        else:
            self.take_screenshot("test_login")
            self.logger.info("*********** Login test is failed *********")
            assert False
        self.driver.close()

    def take_screenshot(self, func_name):
        current_time = datetime.datetime.now().strftime("%d%m%Y")
        file_name = f"{current_time}_{func_name}.png"
        self.driver.save_screenshot(f'{BASE_DIR}/Screenshots/{file_name}')
