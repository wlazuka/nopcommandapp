import time
from selenium.webdriver.support.select import Select
from pageObjects.BasePage import BasePage


class SearchCustomer:
    pass
    txtEmail_xpath = "//input[@id='SearchEmail']"
    txtFirst_Name_xpath = "//input[@id='SearchFirstName']"
    txtLast_Name_xpath = "//input[@id='SearchLastName']"
    btnSearch_xpath = "//button[@id='search-customers']"
    # table_xpath = "//table[@id='customers-grid']"

    def __init__(self, driver):
        self.driver = driver

    def setEmail(self, email):
        self.driver.find_element_by_xpath(self.txtEmail_xpath).clear()
        self.driver.find_element_by_xpath(self.txtEmail_xpath).send_keys(email)

    def setFirstName(self, fname):
        self.driver.find_element_by_xpath(self.txtFirst_Name_xpath).clear()
        self.driver.find_element_by_xpath(self.txtFirst_Name_xpath).send_keys(fname)

    def setLastName(self, lname):
        self.driver.find_element_by_xpath(self.txtLast_Name_xpath).clear()
        self.driver.find_element_by_xpath(self.txtLast_Name_xpath).send_keys(lname)

    def searchClick(self):
        self.driver.find_element_by_xpath(self.btnSearch_xpath).click()

    def searchCustomerByEmail(self, email):
        flag = False
        is_customer = self.driver.find_element_by_xpath(f"//td[contains(text(),'{email}')]")
        if is_customer:
            flag = True
        return flag

    def searchCustomerByName(self, fname, lname):
        flag = False
        is_customer = self.driver.find_element_by_xpath(f"//td[contains(text(),'{fname} {lname}')]")
        if is_customer:
            flag = True
        return flag
