rem Chrome
pytest -s -v -m "regression and sanity" --html=./Reports/report_chrome.html testCases/
rem pytest -s -v -m "sanity" --html=./Reports/report_chrome.html testCases/
rem pytest -s -v -m "regression" --html=./Reports/report_chrome.html testCases/
rem pytest -s -v -m "regression or sanity" --html=./Reports/report_chrome.html testCases/

rem FireFox
rem pytest -s -v -m "sanity" --html=./Reports/report_firefox.html testCases/ --broswer firefox
rem pytest -s -v -m "regression" --html=./Reports/report_firefox.html testCases/ --broswer firefox
rem pytest -s -v -m "regression or sanity" --html=./Reports/report_firefox.html testCases/ --broswer firefox
rem pytest -s -v -m "regression and sanity" --html=./Reports/report_firefox.html testCases/ --broswer firefox
