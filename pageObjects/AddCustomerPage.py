import time
from selenium.webdriver.support.select import Select
from pageObjects.BasePage import BasePage


class AddCustomer:
    lnkCustomers_menu_xpath = "//a[@href='#']//span[contains(text(),'Customers')]"
    lnkCustomers_menu_css = ".treeview:nth-child(4) > a > span"
    lnkCustomers_menuitem_xpath = "//span[@class='menu-item-title'][contains(text(),'Customers')]"
    lnkCustomers_menuitem_css = ".menu-open li:nth-child(1) .menu-item-title"
    btnAddnew_xpath = "//a[@class='btn bg-blue']"
    txtEmail_xpath = "//input[@id='Email']"
    txtPassword_xpath = "//input[@id='Password']"
    txtFirstName_xpath = "//input[@id='FirstName']"
    txtLastName_xpath = "//input[@id='LastName']"
    rdMaleGender_id = "//input[@id='Gender_Male']"
    rdFeMaleGender_id = "//input[@id='Gender_Female']"
    txtDob_xpath = "//input[@id='DateOfBirth']"
    txtCompanyName_xpath = "//input[@id='Company']"
    lstcustomerRoles_xpath = "//ul[@id='SelectedCustomerRoleIds_taglist']"
    lstitemAdministrators_xpath = "//li[contains(text(),'Administrators')]"
    lstitemRegistered_xpath = "//li[contains(text(),'Registered')]"
    lstitemGuests_xpath = "//li[contains(text(),'Guests')]"
    lstitemVendors_xpath = "//li[contains(text(),'Vendors')]"
    drpmgrOfVendor_xpath = "//select[@id='VendorId']"
    txtAdminContent_xpath = "//textarea[@id='AdminComment']"
    btnSave_xpath = "//button[@name='save']"

    def __init__(self, driver):
        self.driver = driver

    def click_on_customer_menu(self):
        # self.driver.find_element_by_xpath(self.lnkCustomers_menu_xpath).click()
        self.driver.find_element_by_css_selector(self.lnkCustomers_menu_css).click()

    def click_on_customer_menu_item(self):
        # self.driver.find_elements_by_xpath(self.lnkCustomers_menuitem_xpath)[0].click()
        self.driver.find_element_by_css_selector(self.lnkCustomers_menuitem_css).click()

    def click_on_add_new(self):
        self.driver.find_element_by_xpath(self.btnAddnew_xpath).click()

    def set_email(self, email):
        self.driver.find_element_by_xpath(self.txtEmail_xpath).send_keys(email)

    def set_password(self, password):
        self.driver.find_element_by_xpath(self.txtPassword_xpath).send_keys(password)

    def set_firstname(self, first_name):
        self.driver.find_element_by_xpath(self.txtFirstName_xpath).send_keys(first_name)

    def set_lastname(self, last_name):
        self.driver.find_element_by_xpath(self.txtLastName_xpath).send_keys(last_name)

    def set_customer_role(self):
        self.driver.find_element_by_xpath(self.lstcustomerRoles_xpath).click()

    def set_customer_roles(self, role):
        self.driver.find_element_by_xpath(self.lstcustomerRoles_xpath).click()
        time.sleep(3)
        if role == "Registered":
            self.listitem = self.driver.find_element_by_xpath(self.lstitemRegistered_xpath)
        elif role == "Administrators":
            self.listitem = self.driver.find_element_by_xpath(self.lstitemAdministrators_xpath)
        elif role == "Vendors":
            self.listitem = self.driver.find_element_by_xpath(self.lstitemVendors_xpath)
        elif role == "Guests":
            # there can be only one role: Registered or Guest
            time.sleep(3)
            self.driver.find_element_by_xpath("//*[@id='SelectedCustomerRoleIds_taglist']/li/span[2]").click()
            self.listitem = self.driver.find_element_by_xpath(self.lstitemGuests_xpath)
        else:
            self.listitem = self.driver.find_element_by_xpath(self.lstitemGuests_xpath)
        time.sleep(3)
        # self.listitem.click()
        self.driver.execute_script("arguments[0].click();", self.listitem)

    def set_manager_of_vendor(self, value):
        drp = Select(self.driver.find_element_by_xpath(self.drpmgrOfVendor_xpath))
        drp.select_by_visible_text(value)

    def set_gender(self, gender):
        if gender == "Male":
            self.driver.find_element_by_id(self.rdMaleGender_id).click()
        elif gender == "Female":
            self.driver.find_element_by_id(self.rdFeMaleGender_id).click()
        else:
            self.driver.find_element_by_id(self.rdMaleGender_id).click()

    def set_dob(self, dob):
        self.driver.find_element_by_xpath(self.txtDob_xpath).send_keys(dob)

    def set_company_name(self, comp_name):
        self.driver.find_element_by_xpath(self.txtCompanyName_xpath).send_keys(comp_name)

    def set_admin_content(self, content):
        self.driver.find_element_by_xpath(self.txtAdminContent_xpath).send_keys(content)

    def click_on_save(self):
        self.driver.find_element_by_xpath(self.btnSave_xpath).click()
