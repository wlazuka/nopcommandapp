import time
import pytest

from pageObjects.LoginPage import LoginPage
from pageObjects.AddCustomerPage import AddCustomer
from pageObjects.SearchCustomer import SearchCustomer
from utilites.readProperities import ReadConfig
from utilites.customLogger import LogGen


class Test_005_SearchCustomer:
    baseURL = ReadConfig.getApplicationURL()
    username = ReadConfig.getUseremail()
    password = ReadConfig.getPassword()
    logger = LogGen.loggen()
    customer_fname = "Victoria"
    customer_lname = "Terces"

    @pytest.mark.regression
    def test_searchCustomerByName(self, setup):
        self.logger.info("***************** Test_005_Search Customer By Name **************")
        self.driver = setup
        self.driver.get(self.baseURL)
        self.driver.maximize_window()

        self.lp = LoginPage(self.driver)
        self.lp.setUserName(self.username)
        self.lp.setPassword(self.password)
        self.lp.clickLogin()
        self.logger.info("************** Login succesful **************")
        self.logger.info("************** Starting Search Customer By Name Test **************")

        self.addcust = AddCustomer(self.driver)
        self.addcust.click_on_customer_menu()
        self.addcust.click_on_customer_menu_item()
        self.logger.info("************** searching customer by First Name ****************")

        search_customer = SearchCustomer(self.driver)
        search_customer.setFirstName(self.customer_fname)
        search_customer.setLastName(self.customer_lname)
        search_customer.searchClick()
        time.sleep(5)
        status = search_customer.searchCustomerByName(self.customer_fname, self.customer_lname)
        assert status == True
        self.logger.info("******* Ending Search Customer by First Name test **********")
        self.driver.close()
