import string
import random
import pytest

from pageObjects.LoginPage import LoginPage
from pageObjects.AddCustomerPage import AddCustomer
from utilites.readProperities import ReadConfig
from utilites.customLogger import LogGen


class Test_003_AddCustomer:
    baseURL = ReadConfig.getApplicationURL()
    username = ReadConfig.getUseremail()
    password = ReadConfig.getPassword()
    logger = LogGen.loggen()

    @pytest.mark.sanity
    def test_add_customer(self, setup):
        self.logger.info("***************** Test_003_AddCustomer **************")
        self.driver = setup
        self.driver.get(self.baseURL)
        self.driver.maximize_window()

        self.lp = LoginPage(self.driver)
        self.lp.setUserName(self.username)
        self.lp.setPassword(self.password)
        self.lp.clickLogin()
        self.logger.info("************** Login succesful **************")
        self.logger.info("************** Starting Add_Customer Test **************")

        self.addcust = AddCustomer(self.driver)
        self.addcust.click_on_customer_menu()
        self.addcust.click_on_customer_menu_item()
        self.addcust.click_on_add_new()
        self.logger.info("************** Providing customer info ****************")

        self.email = random_generator() + "@gmail.com"
        self.addcust.set_email(self.email)
        self.addcust.set_password("test123")
        self.addcust.set_firstname("Wojtek")
        self.addcust.set_lastname("Kowalski")
        self.addcust.set_gender("Male")
        self.addcust.set_dob("7/05/1985")  # Format: D / MM / YYY
        self.addcust.set_company_name("Alatus")
        self.addcust.set_customer_roles("Guests")
        self.addcust.set_manager_of_vendor("Vendor 2")
        self.addcust.set_admin_content("This is for testing.........")
        self.addcust.click_on_save()

        self.logger.info("************* Saving customer info **********")
        self.logger.info("********* Add_Customer validation started *****************")

        self.msg = self.driver.find_element_by_tag_name("body").text

        print(self.msg)
        if 'customer has been added successfully.' in self.msg:
            assert True
            self.logger.info("********* Add customer Test Passed *********")
        else:
            self.driver.save_screenshot("../nopCommandApp/Screenshots/" + "test_addCustomer_scr.png")  # Screenshot
            self.logger.error("********* Add customer Test Failed ************")
            assert False

        self.logger.info("******* Ending Add customer test **********")
        self.driver.close()


def random_generator(size=8, chars=string.ascii_lowercase + string.digits):
    return ''.join(random.choice(chars) for x in range(size))

