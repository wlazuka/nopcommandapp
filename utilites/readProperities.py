import configparser
import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


parser = configparser.RawConfigParser()
res = parser.read(f'{BASE_DIR}/Configurations/config.ini')


class ReadConfig:
    @staticmethod
    def getApplicationURL():
        url = parser.get('main', 'baseURL')
        return url

    @staticmethod
    def getUseremail():
        username = parser.get('main', 'useremail')
        return username

    @staticmethod
    def getPassword():
        password = parser.get('main', 'password')
        return password
