import pytest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options


@pytest.fixture()
def setup(browser):
    options = Options()
    options.binary_location = "/usr/bin/chromium-browser"
    if browser == "safari":
        driver = webdriver.Safari()
        print("Lauching Safari browser......")
    elif browser == "firefox":
        driver = webdriver.Firefox(firefox_profile=None)
        print("Lauching FireFox browser......")
    else:
        driver = webdriver.Chrome(options=options)
        print("Lauching Chrome browser......")
    return driver


def pytest_addoption(parser):
    parser.addoption("--browser")


@pytest.fixture()
def browser(request):
    return request.config.getoption("--browser")


########## PyTest HTML Report

# It is hook for Adding Environment info to HTML Report
def pytest_configure(config):
    config._metadata['Project Name'] = 'nop Commerce'
    config._metadata['Module Name'] = 'Customers'
    config._metadata['Tester'] = 'Wojciech'
